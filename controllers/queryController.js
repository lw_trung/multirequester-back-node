const express = require('express')
const multirq = require('../lib/multiRequest')
const cfg = require('../config/mr.conf')

const controller = express()

module.exports = controller

controller.post('/multiple', (req, res) => {
  const servers = req.body.servers
  const query = req.body.query
  multirq(query, servers).then(result => {
    console.log("multirq: done!")
    res.send(result)
  }).catch(err => {
    console.log('queyControl:err');
  })

})
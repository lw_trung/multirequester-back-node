const svrs = require('../config/mr.conf').dbServers
const util = require('util')

const multiRequest = (query, servers) => {
  let response = {
    results: [],
    errors: []
  }
  let serverPromiseList = []

  servers.forEach(server => {
    const serverCfg = svrs.filter(svr => {
      return server.ip == svr.ip
    })
    if (serverCfg.length == 0) {
      response.errors.push({
        server: server.ip,
        database: null,
        number: null,
        code: "E_CONFIG",
        message: "missing config to connect to the server"
      })
    } else {
      let dbPromiseList = []
      server.databases.forEach(db => {
        const singleRequest = require('./singleRequest')
        const promise = singleRequest(query, serverCfg[0], db)
          .then(result => {
            console.log(serverCfg[0].ip + '---' + db)
            let data = []
            result.recordset.forEach(record => {
              data.push({
                'request-server': server.ip,
                'request-database': db,
                ...record
              })
            });
            return Promise.resolve({
              success: data
            })
          }).catch(err => {
            return Promise.resolve({
              error: {
                'request-server': server.ip,
                'request-database': db,
                number: err.number,
                code: err.code,
                message: err.message
              }
            })
          })
        dbPromiseList.push(promise)
      })
      const serverPromise = Promise.all(dbPromiseList).then(function (answers) {
        console.log('all answer')
        answers.forEach(answer => {
          if (answer.success) {
            response.results = [...response.results, ...answer.success]
          } else {
            response.errors.push(answer.error)
          }
        })
        return Promise.resolve(response)
      }).catch(err => {
        console.log("something wrong")
        return Promise.reject(err)
      })
      serverPromiseList.push(serverPromise)
    }
  })

  return Promise.all(serverPromiseList).then(() => {
    return Promise.resolve(response)
  }).catch(err => {
    console.log("errrr");
  })
}

module.exports = multiRequest;
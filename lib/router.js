const express = require('express')

const queryController = require('../controllers/queryController')
const databaseController = require('../controllers/databaseController')
const testController = require('../controllers/testController')

const router = express()

module.exports = router

router.use('/query', queryController)
router.use('/database', databaseController)
router.use('/test', testController)
// /database
const express = require('express')
const svrs = require('../config/mr.conf').dbServers
const singleRequest = require('../lib/singleRequest')


const controller = express()

module.exports = controller

controller.get('/getAll', (req, res) => {
  const svrPromiseList = []
  let resData = {
    results: [],
    errors: []
  }
  svrs.forEach(svr => {
    const promise = singleRequest("select name from master.sys.databases", svr)
      .then(result => {
        let dbnames = []
        result.recordset.forEach(record => {
          dbnames.push(record.name)
        })
        resData.results.push({
          ip: svr.ip,
          databases: dbnames
        })
        return Promise.resolve()
      })
      .catch(err => {
        resData.errors.push({
          'request-server': svr.ip,
          'request-database': 'master',
          number: err.number,
          code: err.code,
          message: err.message
        })
        return Promise.resolve()
      })
    svrPromiseList.push(promise)
  })

  Promise.all(svrPromiseList)
    .then(() => {
      res.send(resData)
    })
})
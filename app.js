const http = require('http');
const express = require('express');
const cors = require('cors');
const bp = require('body-parser');
const router = require('./lib/router');
const cfg = require('./config/mr.conf')

const app = express();

app.use(cors({
  origin: 'http://localhost:8080',
  credentials: true
}));

app.use(bp.json());
app.use(router);

const server = http.createServer(app)

server.listen(cfg.server.port, cfg.server.host, err => {
  if (err) {
    console.log("[ERROR] server down!")
    return
  }
  console.log('server listen on ' + cfg.server.host + ':' + cfg.server.port)
})
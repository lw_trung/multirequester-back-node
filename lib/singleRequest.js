const mssql = require('mssql')

const singleRequest = (query, server, database = 'master') => {
  // console.log(server.ip + '--' + database)
  const config = {
    user: server.user,
    password: server.password,
    server: server.ip,
    database: database,
    requestTimeout: 999999,
    connectionTimeout: 999999,
    pool: {
      max: 9999,
      min: 0,
      idleTimeoutMillis: 30000
    },
    options: {
      encrypt: false
    }
  }

  const pool = new mssql.ConnectionPool(config)

  let conn = pool

  return conn.connect().then(() => {
    let req = new mssql.Request(conn)
    return req.query(query)
  }).catch(err => {
    console.log('err single request:' + server.ip + ":" + database + ":" + err)
    throw (err);
  })
}
module.exports = singleRequest
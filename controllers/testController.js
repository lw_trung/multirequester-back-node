const express = require('express')
const multirq = require('../lib/multiRequest')
const dbs = require('../config/dbs.json')
const result = require('../config/paramsatos.json')

const controller = express()

module.exports = controller

controller.post('/multiple', (req, res) => {

  res.send({
    results: result,
    errors: {}
  })

})

controller.get('/dbs', (req, res) => {

  res.send(dbs)

})